Code | Country name
:---:|:------------
AF   | Afghanistan
AX   | Aland Islands
AL   | Albania
DZ   | Algeria
VI   | Amer.Virgin Is.
AD   | Andorra
AO   | Angola
AI   | Anguilla
AQ   | Antarctica
AG   | Antigua/Barbuda
AR   | Argentina
AM   | Armenia
AW   | Aruba
AU   | Australia
AT   | Austria
AZ   | Azerbaijan
BS   | Bahamas
BH   | Bahrain
BD   | Bangladesh
BB   | Barbados
BY   | Belarus
BE   | Belgium
BZ   | Belize
BJ   | Benin
BM   | Bermuda
BT   | Bhutan
BO   | Bolivia
BQ   | Bonaire
BA   | Bosnia-Herz.
BW   | Botswana
BV   | Bouvet Island
BR   | Brazil
IO   | Brit.Ind.Oc.Ter
VG   | Brit.Virgin Is.
BN   | Brunei Dar-es-S
BG   | Bulgaria
BF   | Burkina-Faso
BI   | Burundi
KH   | Cambodia
CM   | Cameroon
CA   | Canada
CV   | Cape Verde
KY   | Cayman Islands
CF   | Central Afr.Rep
TD   | Chad
CL   | Chile
CN   | China
CX   | Christmas Islnd
CC   | Coconut Islands
CO   | Colombia
KM   | Comoros
CG   | Congo
CK   | Cook Islands
CR   | Costa Rica
HR   | Croatia
CU   | Cuba
CW   | Curacao
CY   | Cyprus
CZ   | Czech Republic
DK   | Denmark
DJ   | Djibouti
DM   | Dominica
DO   | Dominican Rep.
AN   | Dutch Antilles
EC   | Ecuador
EG   | Egypt
SV   | El Salvador
GQ   | Equatorial Gui.
ER   | Eritrea
EE   | Estonia
ET   | Ethiopia
FK   | Falkland Islnds
FO   | Faroe Islands
FJ   | Fiji
FI   | Finland
FR   | France
PF   | Frenc.Polynesia
GF   | French Guayana
TF   | French S.Territ
GA   | Gabon
GM   | Gambia
GE   | Georgia
DE   | Germany
GH   | Ghana
GI   | Gibraltar
GR   | Greece
GL   | Greenland
GD   | Grenada
GP   | Guadeloupe
GU   | Guam
GT   | Guatemala
GG   | Guernsey
GN   | Guinea
GW   | Guinea-Bissau
GY   | Guyana
HT   | Haiti
HM   | Heard/McDon.Isl
HN   | Honduras
HK   | Hong Kong
HU   | Hungary
IS   | Iceland
IN   | India
ID   | Indonesia
IR   | Iran
IQ   | Iraq
IE   | Ireland
IM   | Isle of Man
IL   | Israel
IT   | Italy
CI   | Ivory Coast
JM   | Jamaica
JP   | Japan
JE   | Jersey
JO   | Jordan
KZ   | Kazakhstan
KE   | Kenya
KI   | Kiribati
KW   | Kuwait
KG   | Kyrgyzstan
LA   | Laos
LV   | Latvia
LB   | Lebanon
LS   | Lesotho
LR   | Liberia
LY   | Libya
LI   | Liechtenstein
LT   | Lithuania
LU   | Luxembourg
MO   | Macau
MK   | Macedonia
MG   | Madagascar
MW   | Malawi
MY   | Malaysia
MV   | Maldives
ML   | Mali
MT   | Malta
MH   | Marshall Islnds
MQ   | Martinique
MR   | Mauretania
MU   | Mauritius
YT   | Mayotte
MX   | Mexico
FM   | Micronesia
UM   | Minor Outl.Ins.
MD   | Moldova
MC   | Monaco
MN   | Mongolia
ME   | Montenegro
MS   | Montserrat
MA   | Morocco
MZ   | Mozambique
MM   | Myanmar
MP   | N.Mariana Islnd
NA   | Namibia
NR   | Nauru
NP   | Nepal
NL   | Netherlands
NC   | New Caledonia
NZ   | New Zealand
NI   | Nicaragua
NE   | Niger
NG   | Nigeria
NU   | Niue Islands
NF   | Norfolk Island
KP   | North Korea
NO   | Norway
OM   | Oman
PK   | Pakistan
PW   | Palau
PS   | Palestine State
PA   | Panama
PG   | Papua Nw Guinea
PY   | Paraguay
PE   | Peru
PH   | Philippines
PN   | Pitcairn Islnds
PL   | Poland
PT   | Portugal
PR   | Puerto Rico
QA   | Qatar
CD   | Republic Congo
RE   | Reunion
RO   | Romania
RU   | Russian Fed.
RW   | Rwanda
GS   | S. Sandwich Ins
ST   | S.Tome,Principe
AS   | Samoa, American
SM   | San Marino
SA   | Saudi Arabia
SN   | Senegal
RS   | Serbia
SC   | Seychelles
SL   | Sierra Leone
SG   | Singapore
SX   | Sint Maarten
SK   | Slovak Republic
SI   | Slovenia
SB   | Solomon Islands
SO   | Somalia
ZA   | South Africa
KR   | South Korea
SS   | South Sudan
ES   | Spain
LK   | Sri Lanka
KN   | St Kitts&Nevis
BL   | St. Barthélemy
SH   | St. Helena
LC   | St. Lucia
MF   | St. Martin
VC   | St. Vincent
PM   | St.Pier,Miquel.
SD   | Sudan
SR   | Suriname
SJ   | Svalbard
SZ   | Swaziland
SE   | Sweden
CH   | Switzerland
SY   | Syria
TW   | Taiwan
TJ   | Tajikistan
TZ   | Tanzania
TH   | Thailand
TL   | Timor-Leste
TG   | Togo
TK   | Tokelau Islands
TO   | Tonga
TT   | Trinidad,Tobago
TN   | Tunisia
TR   | Turkey
TM   | Turkmenistan
TC   | Turksh Caicosin
TV   | Tuvalu
UG   | Uganda
UA   | Ukraine
GB   | United Kingdom
UY   | Uruguay
US   | USA
XK   | USE RS-KL
TP   | USE TL
AE   | Utd.Arab Emir.
UZ   | Uzbekistan
VU   | Vanuatu
VA   | Vatican City
VE   | Venezuela
VN   | Vietnam
WF   | Wallis,Futuna
EH   | West Sahara
WS   | Western Samoa
YE   | Yemen
ZM   | Zambia
ZW   | Zimbabwe
